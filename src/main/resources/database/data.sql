-- Look Up
INSERT INTO look_up(type, name, label)
SELECT 'ComputerType', 'Laptop', 'laptop'
WHERE NOT EXISTS (
    SELECT 1
    FROM look_up
    WHERE type = 'ComputerType'
    AND name = 'Laptop'
);

INSERT INTO look_up(type, name, label)
SELECT 'Language', 'Japanese', '日本語'
WHERE NOT EXISTS (
    SELECT 1
    FROM look_up
    WHERE type = 'Language'
    AND name = 'Japanese'
);

INSERT INTO look_up(type, name, label)
SELECT 'Color', 'Black', 'black'
WHERE NOT EXISTS (
    SELECT 1
    FROM look_up
    WHERE type = 'Color'
    AND name = 'Black'
);

INSERT INTO look_up(type, name, label)
SELECT 'Color', 'Silver', 'silver'
WHERE NOT EXISTS (
    SELECT 1
    FROM look_up
    WHERE type = 'Color'
    AND name = 'Silver'
);

-- Computer Maker
INSERT INTO vendor(type, name, unique_id, website)
SELECT 'ComputerMaker', 'ASUS', 'asus', 'www.asus.com'
WHERE NOT EXISTS (
    SELECT 1
    FROM vendor
    WHERE type = 'ComputerMaker'
    AND unique_id = 'asus'
);

-- SSH Key Vendor
INSERT INTO vendor(type, name, unique_id, website)
SELECT 'SSHKeyVendor', 'Jenkins', 'jenkins', 'www.jenkins.io'
WHERE NOT EXISTS (
    SELECT 1
    FROM vendor
    WHERE type = 'SSHKeyVendor'
    AND unique_id = 'jenkins'
);

-- Computer
INSERT INTO computer(model, computer_type_id, computer_maker_id, language_id)
SELECT 'X507UA',
    (SELECT id FROM look_up WHERE type = 'ComputerType' AND name = 'Laptop'),
    (SELECT id FROM vendor WHERE type = 'ComputerMaker' AND unique_id = 'asus'),
    (SELECT id FROM look_up WHERE type = 'Language' AND name = 'Japanese')
WHERE NOT EXISTS (
    SELECT 1
    FROM computer
    WHERE model = 'X507UA'
);

-- Computer Colors
INSERT INTO computer_colors(computer_id, color_id)
SELECT
    (SELECT id FROM computer WHERE model = 'X507UA'),
    (SELECT id FROM look_up WHERE type = 'Color' AND name = 'Black')
WHERE NOT EXISTS (
    SELECT 1
    FROM computer_colors
    JOIN computer
        ON computer.id = computer_colors.computer_id
        AND computer.model = 'X507UA'
    JOIN look_up color
        ON color.id = computer_colors.color_id
        AND type = 'Color'
        AND name = 'Black'
);

INSERT INTO computer_colors(computer_id, color_id)
SELECT
    (SELECT id FROM computer WHERE model = 'X507UA'),
    (SELECT id FROM look_up WHERE type = 'Color' AND name = 'Silver')
WHERE NOT EXISTS (
    SELECT 1
    FROM computer_colors
    JOIN computer
        ON computer.id = computer_colors.computer_id
        AND computer.model = 'X507UA'
    JOIN look_up color
        ON color.id = computer_colors.color_id
        AND type = 'Color'
        AND name = 'Silver'
);
