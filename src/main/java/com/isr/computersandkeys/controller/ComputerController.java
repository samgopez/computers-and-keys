package com.isr.computersandkeys.controller;

import com.isr.computersandkeys.exception.ErrorCode;
import com.isr.computersandkeys.exception.ForbiddenException;
import com.isr.computersandkeys.exception.NotFoundException;
import com.isr.computersandkeys.model.dto.ComputerDto;
import com.isr.computersandkeys.model.payload.ComputerInfoResponse;
import com.isr.computersandkeys.service.ComputerService;
import com.isr.computersandkeys.service.vendor.ComputerMakerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
@RequestMapping("computers")
@RequiredArgsConstructor
public class ComputerController {

    private final ComputerService computerService;
    private final ComputerMakerService computerMakerService;

    @GetMapping(value = {"{makerUniqueId}", "{makerUniqueId}/{model}"},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ComputerInfoResponse> getComputerInfo(
            @PathVariable String makerUniqueId,
            @PathVariable(required = false) String model) {
        return ResponseEntity.ok(new ComputerInfoResponse(getComputer(makerUniqueId, model)));
    }

    @GetMapping(value = {"{makerUniqueId}", "{makerUniqueId}/{model}"},
            produces = {MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<ComputerDto> getComputerInfoXml(
            @PathVariable String makerUniqueId,
            @PathVariable(required = false) String model) {
        return ResponseEntity.ok(getComputer(makerUniqueId, model));
    }

    private ComputerDto getComputer(String makerUniqueId, String model) {
        if (Objects.isNull(model)) {
            if (computerMakerService.existsByUniqueId(makerUniqueId)) {
                throw new ForbiddenException(ErrorCode.ComputerModelRequired, "Computer model is required to view information.");
            }
            throw new NotFoundException(ErrorCode.ComputerMakerNotExists, "Computer maker " + makerUniqueId + " not found in the system.");
        }

        return computerService.getByMakerAndModel(makerUniqueId, model);
    }
}
