package com.isr.computersandkeys.controller;

import com.isr.computersandkeys.exception.ErrorCode;
import com.isr.computersandkeys.exception.NotFoundException;
import com.isr.computersandkeys.model.dto.SSHKeyDto;
import com.isr.computersandkeys.model.payload.SshKeyRequest;
import com.isr.computersandkeys.service.SSHKeyService;
import com.isr.computersandkeys.service.vendor.SSHKeyVendorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("build-server")
@RequiredArgsConstructor
public class SSHKeyController {

    private final SSHKeyService sshKeyService;
    private final SSHKeyVendorService sshKeyVendorService;

    @PostMapping(value = "{vendorUniqueId}/authorized_keys", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> saveSSHKey(
            @PathVariable String vendorUniqueId,
            @RequestBody @Valid SshKeyRequest sshKeyRequest) {
        if (!sshKeyVendorService.existsByUniqueId(vendorUniqueId)) {
            throw new NotFoundException(ErrorCode.SSHKeyVendorNotFound, "SSH Key vendor " + vendorUniqueId + " not found in the system.");
        }
        SSHKeyDto sshKeyRequestData = sshKeyRequest.getData();
        sshKeyRequestData.setVendorUniqueId(vendorUniqueId);
        sshKeyService.save(sshKeyRequestData);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
