package com.isr.computersandkeys.util;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Utility class for parsing SSH public key
 */
public class SSHKeyUtil {

    private final static int SIZEOF_INT = 4;

    /**
     * This will extract the algorithm from a base64 decoded public key
     *
     * @param publicKey public key that is decoded from base64
     * @return the algorithm of the public key
     */
    public static String getAlgorithm(String publicKey) {
        byte[] decodedPublicKey = Base64.getDecoder().decode(publicKey);
        ByteBuffer byteBuffer = ByteBuffer.wrap(decodedPublicKey);
        AtomicInteger position = new AtomicInteger();
        try {
            return readString(byteBuffer, position);
        } catch (Exception e) {
            // invalid publicKey format
            return null;
        }
    }

    private static String readString(ByteBuffer buffer, AtomicInteger pos) {
        byte[] bytes = readBytes(buffer, pos);
        if (bytes.length == 0) {
            return "";
        }
        return new String(bytes, StandardCharsets.US_ASCII);
    }

    private static byte[] readBytes(ByteBuffer buffer, AtomicInteger pos) {
        int len = buffer.getInt(pos.get());
        byte[] buff = new byte[len];
        for (int i = 0; i < len; i++) {
            buff[i] = buffer.get(i + pos.get() + SIZEOF_INT);
        }
        pos.set(pos.get() + SIZEOF_INT + len);
        return buff;
    }
}
