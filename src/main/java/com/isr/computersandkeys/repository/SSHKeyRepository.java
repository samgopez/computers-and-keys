package com.isr.computersandkeys.repository;

import com.isr.computersandkeys.model.entity.SSHKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SSHKeyRepository extends JpaRepository<SSHKey, Long> {
    Boolean existsByTypeAndPublicKey(String type, String publicKey);
}
