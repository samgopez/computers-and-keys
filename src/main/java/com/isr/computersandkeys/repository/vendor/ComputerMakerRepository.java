package com.isr.computersandkeys.repository.vendor;

import com.isr.computersandkeys.model.entity.vendor.ComputerMaker;
import org.springframework.stereotype.Repository;

@Repository
public interface ComputerMakerRepository extends VendorRepository<ComputerMaker> {
}
