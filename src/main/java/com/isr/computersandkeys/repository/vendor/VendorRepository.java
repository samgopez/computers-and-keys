package com.isr.computersandkeys.repository.vendor;

import com.isr.computersandkeys.model.entity.vendor.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VendorRepository<T extends Vendor> extends JpaRepository<T, Long> {
    Boolean existsByUniqueId(String uniqueId);
    Optional<T> findByUniqueId(String uniqueId);
}
