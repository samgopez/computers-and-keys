package com.isr.computersandkeys.repository.vendor;

import com.isr.computersandkeys.model.entity.vendor.SSHKeyVendor;
import org.springframework.stereotype.Repository;

@Repository
public interface SSHKeyVendorRepository extends VendorRepository<SSHKeyVendor> {
}
