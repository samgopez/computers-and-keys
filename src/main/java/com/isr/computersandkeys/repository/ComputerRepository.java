package com.isr.computersandkeys.repository;

import com.isr.computersandkeys.model.entity.Computer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ComputerRepository extends JpaRepository<Computer, Long> {
    Optional<Computer> findByMakerUniqueIdAndModel(String makerUniqueId, String model);
}
