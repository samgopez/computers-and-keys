package com.isr.computersandkeys.service.vendor.impl;

import com.isr.computersandkeys.model.entity.vendor.Vendor;
import com.isr.computersandkeys.repository.vendor.VendorRepository;

public abstract class AbstractVendorService<T extends Vendor> {
    protected abstract VendorRepository<T> getVendorRepository();

    public boolean existsByUniqueId(String uniqueId) {
        return getVendorRepository().existsByUniqueId(uniqueId);
    }
}
