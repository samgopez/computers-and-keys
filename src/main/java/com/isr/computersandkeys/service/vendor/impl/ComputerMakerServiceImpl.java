package com.isr.computersandkeys.service.vendor.impl;

import com.isr.computersandkeys.model.entity.vendor.ComputerMaker;
import com.isr.computersandkeys.repository.vendor.ComputerMakerRepository;
import com.isr.computersandkeys.repository.vendor.VendorRepository;
import com.isr.computersandkeys.service.vendor.ComputerMakerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ComputerMakerServiceImpl extends AbstractVendorService<ComputerMaker> implements ComputerMakerService {

    private final ComputerMakerRepository computerMakerRepository;

    @Override
    protected VendorRepository<ComputerMaker> getVendorRepository() {
        return computerMakerRepository;
    }
}
