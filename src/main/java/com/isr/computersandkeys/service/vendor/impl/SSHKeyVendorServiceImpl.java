package com.isr.computersandkeys.service.vendor.impl;

import com.isr.computersandkeys.model.entity.vendor.SSHKeyVendor;
import com.isr.computersandkeys.repository.vendor.SSHKeyVendorRepository;
import com.isr.computersandkeys.repository.vendor.VendorRepository;
import com.isr.computersandkeys.service.vendor.SSHKeyVendorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SSHKeyVendorServiceImpl extends AbstractVendorService<SSHKeyVendor> implements SSHKeyVendorService {

    private final SSHKeyVendorRepository sshKeyVendorRepository;

    @Override
    protected VendorRepository<SSHKeyVendor> getVendorRepository() {
        return sshKeyVendorRepository;
    }
}
