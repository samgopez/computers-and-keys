package com.isr.computersandkeys.service.vendor;

public interface VendorService {
    boolean existsByUniqueId(String uniqueId);
}
