package com.isr.computersandkeys.service.impl;

import com.isr.computersandkeys.exception.ErrorCode;
import com.isr.computersandkeys.exception.NotFoundException;
import com.isr.computersandkeys.model.dto.ComputerDto;
import com.isr.computersandkeys.model.dto.adapter.ComputerAdapter;
import com.isr.computersandkeys.repository.ComputerRepository;
import com.isr.computersandkeys.service.ComputerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ComputerServiceImpl implements ComputerService {

    private final ComputerRepository computerRepository;
    private final ComputerAdapter computerAdapter;

    @Override
    public ComputerDto getByMakerAndModel(String computerMakerUniqueId, String computerModel) {
        return computerRepository.findByMakerUniqueIdAndModel(computerMakerUniqueId, computerModel)
                .map(computerAdapter::adapt)
                .orElseThrow(() ->
                        new NotFoundException(ErrorCode.ComputerModelNotFound,
                                "Computer not found with model " + computerModel + " from maker " + computerMakerUniqueId + "."));
    }
}
