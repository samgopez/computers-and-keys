package com.isr.computersandkeys.service.impl;

import com.isr.computersandkeys.exception.BadRequestException;
import com.isr.computersandkeys.exception.ErrorCode;
import com.isr.computersandkeys.exception.ExistsException;
import com.isr.computersandkeys.model.dto.SSHKeyDto;
import com.isr.computersandkeys.model.dto.adapter.SSHKeyAdapter;
import com.isr.computersandkeys.model.entity.SSHKey;
import com.isr.computersandkeys.repository.SSHKeyRepository;
import com.isr.computersandkeys.repository.vendor.SSHKeyVendorRepository;
import com.isr.computersandkeys.service.SSHKeyService;
import com.isr.computersandkeys.util.SSHKeyUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SSHKeyServiceImpl implements SSHKeyService {

    private final SSHKeyRepository sshKeyRepository;
    private final SSHKeyVendorRepository sshKeyVendorRepository;
    private final SSHKeyAdapter sshKeyAdapter;

    @Override
    public void save(SSHKeyDto sshKeyDto) {
        validateKey(sshKeyDto);

        if (sshKeyRepository.existsByTypeAndPublicKey(sshKeyDto.getType(), sshKeyDto.getPublicKey())) {
            throw new ExistsException(ErrorCode.SSHKeyExists, "SSH Key already exists");
        }

        SSHKey sshKeyToSave = sshKeyAdapter.adapt(sshKeyDto);
        sshKeyVendorRepository.findByUniqueId(sshKeyDto.getVendorUniqueId())
                .ifPresent(sshKeyToSave::setVendor);
        sshKeyRepository.save(sshKeyToSave);
    }

    @Override
    public void validateKey(SSHKeyDto sshKeyDto) {
        String algorithm = SSHKeyUtil.getAlgorithm(sshKeyDto.getPublicKey());
        if (!sshKeyDto.getType().equals(algorithm)) {
            throw new BadRequestException(ErrorCode.InvalidKeyContent, "The content of the public key is invalid for the type '" + sshKeyDto.getType() + "'");
        }
    }
}