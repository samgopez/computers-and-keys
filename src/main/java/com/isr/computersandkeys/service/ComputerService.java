package com.isr.computersandkeys.service;

import com.isr.computersandkeys.model.dto.ComputerDto;

public interface ComputerService {
    ComputerDto getByMakerAndModel(String computerMakerUniqueId, String computerModel);
}
