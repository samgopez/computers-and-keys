package com.isr.computersandkeys.service;

import com.isr.computersandkeys.model.dto.SSHKeyDto;

public interface SSHKeyService {
    void save(SSHKeyDto sshKeyDto);
    void validateKey(SSHKeyDto sshKeyDto);
}
