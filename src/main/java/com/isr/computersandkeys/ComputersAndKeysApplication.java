package com.isr.computersandkeys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComputersAndKeysApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComputersAndKeysApplication.class, args);
    }

}
