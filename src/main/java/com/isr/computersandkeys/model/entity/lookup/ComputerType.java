package com.isr.computersandkeys.model.entity.lookup;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("ComputerType")
@Data
@EqualsAndHashCode(callSuper = true)
public class ComputerType extends LookUp {
}
