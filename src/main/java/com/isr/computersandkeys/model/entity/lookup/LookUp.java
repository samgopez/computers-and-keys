package com.isr.computersandkeys.model.entity.lookup;

import com.isr.computersandkeys.model.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "look_up",
        uniqueConstraints = { @UniqueConstraint(columnNames = { "type", "name" }) })
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@Data
@EqualsAndHashCode(callSuper = true)
public class LookUp extends BaseEntity {

    @Column(insertable = false, updatable = false)
    private String type;

    @Column(nullable = false)
    private String name;

    private String label;
}
