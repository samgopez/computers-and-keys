package com.isr.computersandkeys.model.entity.vendor;

import com.isr.computersandkeys.model.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "vendor",
        uniqueConstraints = { @UniqueConstraint(columnNames = { "type", "name" }) })
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@Data
@EqualsAndHashCode(callSuper = true)
public class Vendor extends BaseEntity {
    @Column(insertable = false, updatable = false)
    private String type;
    @Column(nullable = false)
    private String name;
    @Column(name = "unique_id", unique = true, nullable = false)
    private String uniqueId;
    private String website;
}
