package com.isr.computersandkeys.model.entity.lookup;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Color")
@Data
@EqualsAndHashCode(callSuper = true)
public class Color extends LookUp {
}
