package com.isr.computersandkeys.model.entity;

import com.isr.computersandkeys.model.entity.lookup.Color;
import com.isr.computersandkeys.model.entity.lookup.ComputerType;
import com.isr.computersandkeys.model.entity.lookup.Language;
import com.isr.computersandkeys.model.entity.vendor.ComputerMaker;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "computer")
@Data
@EqualsAndHashCode(callSuper = true)
public class Computer extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "computer_type_id", nullable = false)
    private ComputerType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "computer_maker_id", nullable = false)
    private ComputerMaker maker;

    @Column(unique = true, nullable = false)
    private String model;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "language_id", nullable = false)
    private Language language;

    @ManyToMany
    @JoinTable(name = "computer_colors",
            joinColumns = @JoinColumn(name = "computer_id"),
            inverseJoinColumns = @JoinColumn(name = "color_id"),
            uniqueConstraints = @UniqueConstraint(columnNames = { "computer_id", "color_id" }))
    private List<Color> colors = new ArrayList<>();
}
