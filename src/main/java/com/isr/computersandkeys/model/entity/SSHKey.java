package com.isr.computersandkeys.model.entity;

import com.isr.computersandkeys.model.entity.vendor.SSHKeyVendor;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "ssh_key")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SSHKey extends BaseEntity {
    private String type;
    @Lob
    @Column(name = "public_key")
    private String publicKey;
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vendor_id")
    private SSHKeyVendor vendor;
}
