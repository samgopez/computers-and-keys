package com.isr.computersandkeys.model.entity.lookup;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Language")
@Data
@EqualsAndHashCode(callSuper = true)
public class Language extends LookUp {
}
