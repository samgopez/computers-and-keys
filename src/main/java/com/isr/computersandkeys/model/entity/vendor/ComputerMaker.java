package com.isr.computersandkeys.model.entity.vendor;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("ComputerMaker")
@Data
@EqualsAndHashCode(callSuper = true)
public class ComputerMaker extends Vendor {

}
