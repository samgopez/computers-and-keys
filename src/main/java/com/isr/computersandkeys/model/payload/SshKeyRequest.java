package com.isr.computersandkeys.model.payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.isr.computersandkeys.model.dto.SSHKeyDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SshKeyRequest {
    @Valid
    @JsonProperty("ssh-key")
    private SSHKeyDto data;
}
