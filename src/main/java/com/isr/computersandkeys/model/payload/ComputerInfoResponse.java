package com.isr.computersandkeys.model.payload;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.isr.computersandkeys.model.dto.ComputerDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JacksonXmlRootElement(localName = "computer")
public class ComputerInfoResponse {
    private ComputerDto computer;
}
