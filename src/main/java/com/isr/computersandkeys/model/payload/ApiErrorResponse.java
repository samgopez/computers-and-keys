package com.isr.computersandkeys.model.payload;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
@JacksonXmlRootElement(localName = "error")
public class ApiErrorResponse {
    private final String code;
    private final String message;
}
