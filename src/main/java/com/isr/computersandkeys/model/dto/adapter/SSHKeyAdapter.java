package com.isr.computersandkeys.model.dto.adapter;

import com.isr.computersandkeys.model.dto.SSHKeyDto;
import com.isr.computersandkeys.model.entity.SSHKey;
import org.springframework.stereotype.Component;

@Component
public class SSHKeyAdapter {

    public SSHKey adapt(SSHKeyDto fromSSHKeyDto) {
        return SSHKey.builder()
                .type(fromSSHKeyDto.getType())
                .publicKey(fromSSHKeyDto.getPublicKey())
                .comment(fromSSHKeyDto.getComment())
                .build();
    }
}
