package com.isr.computersandkeys.model.dto.adapter;

import com.isr.computersandkeys.model.dto.ColorsDto;
import com.isr.computersandkeys.model.dto.ComputerDto;
import com.isr.computersandkeys.model.entity.Computer;
import com.isr.computersandkeys.model.entity.lookup.Color;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ComputerAdapter {

    public ComputerDto adapt(Computer computer) {
        ColorsDto colors = new ColorsDto(
                computer.getColors().stream()
                        .map(Color::getLabel).collect(Collectors.toList()));
        return ComputerDto.builder()
                .model(computer.getModel())
                .type(computer.getType().getLabel())
                .maker(computer.getMaker().getName())
                .language(computer.getLanguage().getLabel())
                .colors(colors)
                .build();
    }
}
