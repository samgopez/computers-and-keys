package com.isr.computersandkeys.model.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
public class ColorsDto {
    private final List<String> color;
}
