package com.isr.computersandkeys.model.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JacksonXmlRootElement(localName = "computer")
public class ComputerDto {
    private String type;
    private String maker;
    private String model;
    private String language;
    private ColorsDto colors;
}