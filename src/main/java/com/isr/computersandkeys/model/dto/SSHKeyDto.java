package com.isr.computersandkeys.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SSHKeyDto {
    @NotBlank
    private String type;
    @JsonProperty("public")
    @NotBlank
    private String publicKey;
    private String comment;
    @JsonIgnore
    private String vendorUniqueId;
}
