package com.isr.computersandkeys.exception;

import org.springframework.http.HttpStatus;

public class ForbiddenException extends AbstractException {
    public ForbiddenException(ErrorCode code, String message) {
        super(HttpStatus.FORBIDDEN, code, message);
    }
}
