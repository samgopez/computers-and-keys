package com.isr.computersandkeys.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class AbstractException extends RuntimeException {
    private final HttpStatus httpStatus;
    private final ErrorCode code;

    public AbstractException(HttpStatus httpStatus, ErrorCode code, String message) {
        super(message);
        this.code = code;
        this.httpStatus = httpStatus;
    }

    public String getCodeAsString() {
        return this.code.name();
    }
}
