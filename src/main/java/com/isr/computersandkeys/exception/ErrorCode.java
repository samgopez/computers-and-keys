package com.isr.computersandkeys.exception;

public enum ErrorCode {
    ComputerMakerNotExists,
    ComputerModelNotFound,
    ComputerModelRequired,
    SSHKeyVendorNotFound,
    SSHKeyExists,
    InvalidKeyContent
}
