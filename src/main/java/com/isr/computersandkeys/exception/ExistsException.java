package com.isr.computersandkeys.exception;

import org.springframework.http.HttpStatus;

public class ExistsException extends AbstractException {
    public ExistsException(ErrorCode code, String message) {
        super(HttpStatus.CONFLICT, code, message);
    }
}
