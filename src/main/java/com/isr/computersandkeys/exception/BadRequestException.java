package com.isr.computersandkeys.exception;

import org.springframework.http.HttpStatus;

public class BadRequestException extends AbstractException {
    public BadRequestException(ErrorCode code, String message) {
        super(HttpStatus.BAD_REQUEST, code,  message);
    }
}
