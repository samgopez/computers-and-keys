package com.isr.computersandkeys.exception.handler;

import com.isr.computersandkeys.exception.AbstractException;
import com.isr.computersandkeys.model.payload.ApiErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Slf4j
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { AbstractException.class })
    public ResponseEntity<?> handleCustomException(AbstractException exception) {
        logger.debug(exception.getMessage());
        return ResponseEntity
                .status(exception.getHttpStatus())
                .body(new ApiErrorResponse(exception.getCodeAsString(), exception.getMessage()));
    }
}
